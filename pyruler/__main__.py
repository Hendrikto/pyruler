from pynput import mouse

from pyruler import on_click

if __name__ == '__main__':
    with mouse.Listener(on_click=on_click, suppress=True) as listener:
        listener.join()
