from math import sqrt

from pynput import mouse


class State():
    start_x = None
    start_y = None


def on_click(x, y, button, pressed, state=State):
    if button is not mouse.Button.left:
        return
    if pressed:
        state.start_x = x
        state.start_y = y
    else:
        width = abs(x - state.start_x) + 1
        height = abs(y - state.start_y) + 1
        diagonal = sqrt(width ** 2 + height ** 2)
        print('\n'.join([
            f'X: {state.start_x} - {x}',
            f'Y: {state.start_y} - {y}',
            f'Width: {width}',
            f'Height: {height}',
            f'Diagonal: {diagonal:.2f}',
        ]))
        return False
